# README #

This python script should run with python 2.7 and 3.5. Its works with windows and linux. In order to work properly, your series folder should look like this :

```
#!python

series
|-- watchNext.py
|-- Arrow
      |-- season 1
            |-- *1.*
            |-- *2.*
           ..... the names does not matter, the episodes must only be sort in alphabetical order
           ..... and there should only be the episodes, no other files ! 
      |-- season 2
        ....
|-- Vixen
      .... same here
|-- Flash
      ... and here

```

If there is a serie you don't want to watch, just don't download it and the missing episodes should be skipped.

How it works : the order of the episodes is hard coded in the script. each time you launch the script a line is added in the savefile "DCComicsSeries.txt". The name of the episodes you watched is written for your convenience but what the script looks at is the length of the file. The script launches the good episode using command line and the path (ex : Arrow/season1/[3rdEpisode], hence the alphabetical order and the folder naming constraint) so it should launch your default video player with the file path in argument.