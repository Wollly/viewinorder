#!/usr/bin/python
# -*- encoding: <UTF8> -*-

"""
script source : http://watch-dc-comics-in-order.sourceforge.net

source : http://www.imdb.com/list/ls074499769/


Arrow Seasons 1 & 2;
*Arrow Season 1 (1 - 23)
*Arrow Season 2 (1 - 23)

Arrow Season 3 & The Flash Season 1 & Vixen Web Series;
*The Flash 1x01: Pilot
*Arrow 3x01: The Calm
*Arrow 3x02: Sara
*The Flash 1x02: Fastest Man Alive
*The Flash 1x03: Things You Can't Outrun
*Arrow 3x03: Corto Maltese
*The Flash 1x04: Going Rogue
*Arrow 3x04: The Magician
*Arrow 3x05: The Secret Origin of Felicity Smoak
*The Flash 1x05: Plastique
*Arrow 3x06: Guilty
*The Flash 1x06: The Flash Is Born
*Arrow 3x07: Draw Back Your Bow
*The Flash 1x07: Power Outage
*The Flash 1x08: Flash vs. Arrow
*Arrow 3x08: The Brave and the Bold
*The Flash 1x09: The Man in the Yellow Suit
*Arrow 3x09: The Climb
*The Flash 1x10: Revenge of the Rogues
*Arrow 3x10: Left Behind
*The Flash 1x11: The Sound and the Fury
*Arrow 3x11: Midnight City
*The Flash 1x12: Crazy for You
*Arrow 3x12: Uprising
*The Flash 1x13: The Nuclear Man
*The Flash 1x14: Fallout
*Arrow 3x13: Canaries
*Arrow 3x14: The Return

*Vixen 1x01
*Vixen 1x02
*Vixen 1x03
*Vixen 1x04
*Vixen 1x05
*Vixen 1x06

*Arrow 3x15: Nanda Parbat
*Arrow 3x16: The Offer
*The Flash 1x15: Out of Time
*The Flash 1x16: Rogue Time
*The Flash 1x17: Tricksters
*Arrow 3x17: Suicidal Tendencies
*Arrow 3x18: Public Enemy
*The Flash 1x18: All Star Team Up
*Arrow 3x19: Broken Arrow
*Arrow 3x20: The Fallen
*The Flash 1x19: Who Is Harrison Wells?
*The Flash 1x20: The Trap
*The Flash 1x21: Grodd Lives
*Arrow 3x21: Al Sah-him
*Arrow 3x22: This Is Your Sword
*The Flash 1x22: Rogue Air
*Arrow 3x23: My Name Is Oliver Queen
*The Flash 1x23: Fast Enough

Constantine Season 1
*Constantine Season 1 (1 - 13)

Arrow Season 4 & The Flash Season 2 & Legends of Tomorrow Season 1
*Arrow 4x01: Green Arrow
*The Flash 2x01: The Man Who Saved Central City
*The Flash 2x02: Flash of Two Worlds
*Arrow 4x02: The Candidate
*The Flash 2x03: Family of Rogues
*Arrow 4x03: Restoration
*Arrow 4x04: Beyond Redemption
*The Flash 2x04: The Fury of Firestorm
*The Flash 2x05: The Darkness and the Light
*Arrow 4x05: Haunted
*Arrow 4x06: Lost Souls
*The Flash 2x06: Enter Zoom
*Arrow 4x07: Brotherhood
*The Flash 2x07: Gorilla Warfare
*The Flash 2x08: Legends of Today
*Arrow 4x08: Legends of Yesterday
*Arrow 4x09: Dark Waters
*The Flash 2x09: Running to Stand Still
*The Flash 2x10: Potential Energy
*Arrow 4x10: Blood Debts
*Legends of Tomorrow 1x01: Pilot, Part 1
*Legends of Tomorrow 1x01: Pilot, Part 2
*The Flash 2x11: The Reverse-Flash Returns
*Arrow 4x11: A.W.O.L
*The Flash 2x12: Fast Lane
*Arrow 4x12: Unchained
*Arrow 4x13: Sins of the Father
*The Flash 2x13: Welcome to Earth-2
*The Flash 2x14: Escape from Earth-2
*The Flash 2x15: King Shark
*Supergirl 1x1: Pilot
*Supergirl 1x2: Stronger Together
*Supergirl 1x3: Fight or Flight
*Supergirl 1x5: How Does She Do It?
*Supergirl 1x4: Livewire
*Supergirl 1x6: Redfaced
*Supergirl 1x7: Human for a Day
*Supergirl 1x8: Hostile Takeover
*Supergirl 1x9: Blood Bonds
*Supergirl 1x10: Childish Things
*Supergirl 1x11: Strange Visitor from Another Planet
*Supergirl 1x12: Bizarro
*Supergirl 1x13: For the Girl Who Has Everything
*Supergirl 1x14: Truth, Justice and the American Way
*Supergirl 1x15: Solitude
*Supergirl 1x16: Falling 
*Supergirl 1x17: Manhunter
*Legends of Tomorrow 1x04: White Knights
*Legends of Tomorrow 1x05: Fail-Safe
*Legends of Tomorrow 1x06: Star City 2046
*Arrow 4x14: Code of Silence
*Arrow 4x15: Taken
*Legends of Tomorrow 1x07: Marooned
*Legends of Tomorrow 1x08: Night of the Hawk
*Legends of Tomorrow 1x09: Left Behind
*The Flash 2x16: Trajectory
*Arrow 4x16: Broken Hearts
*The Flash 2x17: Flash Back
*Supergirl 1x18: Worlds Finest
*Supergirl 1x19: Myriad
*Supergirl 1x20: Better Angels
*Arrow 4x17: Beacon of Hope
*Arrow 4x18: Eleven-Fifty-Nine
*Legends of Tomorrow 1x10: Progeny
*The Flash 2x18: Versus Zoom
*Supergirl 1x18: Worlds Finest
*Supergirl 1x19: Myriad
*Supergirl 1x20: Better Angels
*Supergirl 2x01: The Adventures of Supergirl
*Legends of Tomorrow 1x11: The Magnificent Eight
*Legends of Tomorrow 1x12: Last Refuge
*The Flash 2x19: Back to Normal
*Supergirl 2x02: The Last Children of Krypton
*Supergirl 2x03: Welcome to Earth
*Legends of Tomorrow 1x13: Leviathan
*Legends of Tomorrow 1x14: River of Time
*Legends of Tomorrow 1x15: Destiny
*Supergirl 2x04: Survivors
*Arrow 4x20: Genesis
*Arrow 4x21: Monument Point
*Arrow 4x22: Lost in the Flood
*Supergirl 2x05: Crossfire
*Legends of Tomorrow 1x16: Legendary
*Arrow 4x23: Schism
*The Flash 2x20: Rupture
*The Flash 2x21: The Runaway Dinosaur
*The Flash 2x22: Invincible
*The Flash 2x23: The Race of His Life

Arrow Season 5 / The Flash Season 3 / Legends of Tomorrow Season 2 / Supergirl Season 2 / Freedom Fighters: The Ray
*Supergirl 2x06: Changing
*The Flash 3x01: Flashpoint
*The Flash 3x02: Paradox
*The Flash 3x03: Magenta
*The Flash 3x04: The New Rogues
*The Flash 3x05: Monster
*The Flash 3x06: Shade
*The Flash 3x07: Killer Frost
*Supergirl 2x07: The Darkest Place
*Arrow 5x01: Legacy
*Arrow 5x02: The Recruits
*Legends of Tomorrow 2x01: Out of Time
*Legends of Tomorrow 2x02: The Justice Society of America
*Legends of Tomorrow 2x03: Shogun
*Arrow 5x03: A Matter of Trust
*Arrow 5x04: Penance
*Arrow 5x05: Human Target
*Legends of Tomorrow 2x04: Abominations
*Arrow 5x06: So It Begins
*Legends of Tomorrow 2x05: Compromised
*Arrow 5x07: Vigilante
*Legends of Tomorrow 2x06: Outlaw Country
*Supergirl 2x08: Medusa
*The Flash 3x08: Invasion!
*Arrow 5x08: Invasion!
*Legends of Tomorrow 2x07: Invasion!
*The Flash 3x09: The Present
*Arrow 5x09: What We Leave Behind
*Arrow 5x10: Who Are You?
*Legends of Tomorrow 2x08: The Chicago Way
*Legends of Tomorrow 2x09: Raiders of the Lost Art
*Legends of Tomorrow 2x10: The Legion of Doom
*Legends of Tomorrow 2x11: Turncoat
*Supergirl 2x09: Supergirl Lives
*The Flash 3x10: Borrowing Problems from the Future
*Supergirl 2x10: We Can Be Heroes
*The Flash 3x11: Dead or Alive
*Arrow 5x11: Second Chances
*Supergirl 2x11: The Martian Chronicles
*The Flash 3x12: Untouchable
*Arrow 5x12: Bratva
*Supergirl 2x12: Luthors
*Supergirl 2x13: Mr. & Mrs. Mxyzptlk
*Supergirl 2x14: Homecoming
*The Flash 3x13: Attack on Gorilla City
*The Flash 3x14: Attack on Central City
*Arrow 5x13: Spectre of the Gun
*Legends of Tomorrow 2x12: Camelot/3000
*Legends of Tomorrow 2x13: Land of the Lost 
*Arrow 5x14: The Sin-Eater
*Arrow 5x15: Fighting Fire with Fire
*Supergirl 2x15: Exodus
*The Flash 3x15: The Wrath of Savitar
*The Flash 3x16: Into the Speed Force
*Legends of Tomorrow 2x14: Moonshot
*Arrow 5x16: Checkmate
*Supergirl 2x16: Star-Crossed
*The Flash 3x17: Duet
*Legends of Tomorrow 2x15: Fellowship of the Spear
*Arrow 5x17: Kapiushon
*Supergirl 2x17: Distant Sun 
*The Flash 3x18: Abra Kadabra
*The Flash 3x19: The Once and Future Flash
*The Flash 3x20: I Know Who You Are
*Arrow 5x19: Dangerous Liaisons
*Arrow 5x20: Underneath
*Legends of Tomorrow 2x16: Doomworld
*Legends of Tomorrow 2x17: Aruba
*Supergirl 2x18: Ace Reporter
*Supergirl 2x19: Alex

"""

import os, sys
import subprocess
import time
import webbrowser as wb

LINE_TEMPLATE_DONE = "[x] {} {}x{}\n"
LINE_TEMPLATE_MISSING = "[ ] {} {}x{}\n"
SAVE_FILE = "DCComicsSeries.txt"
SERIES_ROOT = os.getcwd()
DWNLD_URL = "https://sourceforge.net/projects/watch-dc-comics-in-order/files/latest/download?source=navbar"

def gt(word1, word2):
    # returns True if word1 is greater than word2 <=> closer to z than to a
    minlen = min(len(word1), len(word2))
    for i in range(minlen):
        if (word1[i] > word2[i]):
            return True
        elif (word1[i] < word2[i]):
            return False
    # at this point, both words are identical. The lengthiest is therefore the greatest
    return len(word1) > len(word2)

def ls(path):
    # returns the elements contained in path sorted in alphabetical order
    lst = os.listdir(path)
    permut = True
    while permut != False:
        permut = False
        for elemNb in range(len(lst)-1):
            elemn = lst[elemNb]
            elemnp1 = lst[elemNb+1]
            if (gt(elemn, elemnp1)):
                temp = lst[elemNb]
                lst[elemNb] = lst[elemNb+1]
                lst[elemNb+1] = temp
                permut = True
    return lst

class Serie:
    def __init__(self, name, folder):
        self.name = name
        self.folder = folder
    
class Episode:
    def __init__(self, serie, season, number):
        if (serie.__class__.__name__ == "Serie"):
            self.serie = serie
        else:
            raise TypeError("serie attribute is supposed to be of class Serie")

        self.season = season
        self.number = number
    
    def play(self, playWith=""):
        # move to the episode
        os.chdir(self.serie.folder)
        os.chdir("Season "+str(self.season))
        lst = ls(".")
        if (self.number <= len(lst)):
            save_progress(self)
            # watch it !
            os.system(playWith+' "'+lst[self.number-1]+'"')
        else:
            save_progress(self, missing=True)
            print(self.serie.name + " Season " + str(self.season) + 
                    " Episode " + str(self.number) + " missing")

def save_progress(episode, missing=False):
        # appends the current episode to the 'done' list
        f = open(SERIES_ROOT+'/'+SAVE_FILE, 'a')
        if (missing):
                print("missing")
                f.write(LINE_TEMPLATE_MISSING.format(episode.serie.name, episode.season, episode.number))
        else:
                print("done")
                f.write(LINE_TEMPLATE_DONE.format(episode.serie.name, episode.season, episode.number))
        f.close()
        return
                
        
arrow = Serie("The Arrow", "Arrow")
flash = Serie("The Flash", "Flash")
vixen = Serie("Vixen", "Vixen")
legends = Serie("Legends of Tomorrow", "Legends of Tomorrow")
supergirl = Serie("Super Girl", "Super Girl")

# let's create the folders for the user :)
def make_folder(name):
    if not(os.path.exists(name)):
        print(name+" -- created")
        os.makedirs(name, exist_ok = True)

def make_serieFolder(root_name, nb_seasons):
    make_folder(root_name)
    for i in range(1, nb_seasons+1):
        make_folder(root_name+"/Season "+str(i))

make_serieFolder("Arrow", 4)
make_serieFolder("Flash", 2)
make_serieFolder("Vixen", 1)
make_serieFolder("Legends of Tomorrow", 1)
make_serieFolder("Super Girl", 1)

liste = [
# Arrow Season 1
    Episode(arrow, 1, 1),
    Episode(arrow, 1, 2),
    Episode(arrow, 1, 3),
    Episode(arrow, 1, 4),
    Episode(arrow, 1, 5),
    Episode(arrow, 1, 6),
    Episode(arrow, 1, 7),
    Episode(arrow, 1, 8),
    Episode(arrow, 1, 9),
    Episode(arrow, 1, 10),
    Episode(arrow, 1, 11),
    Episode(arrow, 1, 12),
    Episode(arrow, 1, 13),
    Episode(arrow, 1, 14),
    Episode(arrow, 1, 15),
    Episode(arrow, 1, 16),
    Episode(arrow, 1, 17),
    Episode(arrow, 1, 18),
    Episode(arrow, 1, 19),
    Episode(arrow, 1, 20),
    Episode(arrow, 1, 21),
    Episode(arrow, 1, 22),
    Episode(arrow, 1, 23),
# Arrow Season 2
    Episode(arrow, 2, 1),
    Episode(arrow, 2, 2),
    Episode(arrow, 2, 3),
    Episode(arrow, 2, 4),
    Episode(arrow, 2, 5),
    Episode(arrow, 2, 6),
    Episode(arrow, 2, 7),
    Episode(arrow, 2, 8),
    Episode(arrow, 2, 9),
    Episode(arrow, 2, 10),
    Episode(arrow, 2, 11),
    Episode(arrow, 2, 12),
    Episode(arrow, 2, 13),
    Episode(arrow, 2, 14),
    Episode(arrow, 2, 15),
    Episode(arrow, 2, 16),
    Episode(arrow, 2, 17),
    Episode(arrow, 2, 18),
    Episode(arrow, 2, 19),
    Episode(arrow, 2, 20),
    Episode(arrow, 2, 21),
    Episode(arrow, 2, 22),
# Arrow Season 3, Flash season 1 and Vixen    
    Episode(flash, 1, 1),
    Episode(arrow, 3, 1),
    Episode(arrow, 3, 2),
    Episode(flash, 1, 2),
    Episode(flash, 1, 3),
    Episode(arrow, 3, 3),
    Episode(flash, 1, 4),
    Episode(arrow, 3, 4),
    Episode(arrow, 3, 5),
    Episode(flash, 1, 5),
    Episode(arrow, 3, 6),
    Episode(flash, 1, 6),
    Episode(arrow, 3, 7),
    Episode(flash, 1, 7),
    Episode(flash, 1, 8),
    Episode(arrow, 3, 8),
    Episode(flash, 1, 9),
    Episode(arrow, 3, 9),
    Episode(flash, 1, 10),
    Episode(arrow, 3, 10),
    Episode(flash, 1, 11),
    Episode(arrow, 3, 11),
    Episode(flash, 1, 12),
    Episode(arrow, 3, 12),
    Episode(flash, 1, 13),
    Episode(flash, 1, 14),
    Episode(arrow, 3, 13),
    Episode(arrow, 3, 14), # pi \0/
    Episode(vixen, 1, 1),
    Episode(vixen, 1, 2),
    Episode(vixen, 1, 3),
    Episode(vixen, 1, 4),
    Episode(vixen, 1, 5),
    Episode(vixen, 1, 6),
    Episode(arrow, 3, 15),
    Episode(arrow, 3, 16),
    Episode(flash, 1, 15),
    Episode(flash, 1, 16),
    Episode(flash, 1, 17),
    Episode(arrow, 3, 17),
    Episode(arrow, 3, 18),
    Episode(flash, 1, 18),
    Episode(arrow, 3, 19),
    Episode(arrow, 3, 20),
    Episode(flash, 1, 19),
    Episode(flash, 1, 20),
    Episode(flash, 1, 21),
    Episode(arrow, 3, 21),
    Episode(arrow, 3, 22),
    Episode(flash, 1, 22),
    Episode(arrow, 3, 23),
    Episode(flash, 1, 23),
# insert constantine here
    Episode(arrow, 4, 1),
    Episode(flash, 2, 1),
    Episode(flash, 2, 2),
    Episode(arrow, 4, 2),
    Episode(flash, 2, 3),
    Episode(arrow, 4, 3),
    Episode(arrow, 4, 4),
    Episode(flash, 2, 4),
    Episode(flash, 2, 5),
    Episode(arrow, 4, 5),
    Episode(arrow, 4, 6),
    Episode(flash, 2, 6),
    Episode(arrow, 4, 7),
    Episode(flash, 2, 7),
    Episode(flash, 2, 8),
    Episode(arrow, 4, 8),
    Episode(arrow, 4, 9),
    Episode(flash, 2, 9),
    Episode(flash, 2, 10),
    Episode(arrow, 4, 10),
    Episode(legends, 1, 1),
    Episode(legends, 1, 2),
    Episode(flash, 2, 11),
    Episode(arrow, 4, 11),
    Episode(flash, 2, 12),
    Episode(arrow, 4, 12),
    Episode(arrow, 4, 13),
    Episode(flash, 2, 13),
    Episode(flash, 2, 14),
    Episode(flash, 2, 15),
    Episode(supergirl, 1, 1),
    Episode(supergirl, 1, 2),
    Episode(supergirl, 1, 3),
    Episode(supergirl, 1, 4),
    Episode(supergirl, 1, 5),
    Episode(supergirl, 1, 6),
    Episode(supergirl, 1, 7),
    Episode(supergirl, 1, 8),
    Episode(supergirl, 1, 9),
    Episode(supergirl, 1, 10),
    Episode(supergirl, 1, 11),
    Episode(supergirl, 1, 12),
    Episode(supergirl, 1, 13),
    Episode(supergirl, 1, 14),
    Episode(supergirl, 1, 15),
    Episode(supergirl, 1, 16),
    Episode(supergirl, 1, 17),
    Episode(legends, 1, 4),
    Episode(legends, 1, 5),
    Episode(legends, 1, 6),
    Episode(arrow, 4, 14),
    Episode(arrow, 4, 15),
    Episode(legends, 1, 7),
    Episode(legends, 1, 8),
    Episode(legends, 1, 9),
    Episode(flash, 2, 16),
    Episode(arrow, 4, 16),
    Episode(flash, 2, 17),
    Episode(supergirl, 1, 18),
    Episode(supergirl, 1, 19),
    Episode(supergirl, 1, 20),
    Episode(supergirl, 2, 1),
    Episode(legends, 1, 11),
    Episode(legends, 1, 12),
    Episode(flash, 2, 19),
    Episode(supergirl, 2, 2),
    Episode(supergirl, 2, 3),
    Episode(legends, 1, 13),
    Episode(legends, 1, 14),
    Episode(legends, 1, 15),
    Episode(supergirl, 2, 4),
    Episode(arrow, 4, 20),
    Episode(arrow, 4, 21),
    Episode(arrow, 4, 22),
    Episode(supergirl, 2, 5),
    Episode(legends, 1, 16),
    Episode(arrow, 4, 23),
    Episode(flash, 2, 20),
    Episode(flash, 2, 21),
    Episode(flash, 2, 22),
    Episode(flash, 2, 23),
#Arrow Season 5 / The Flash Season 3 / Legends of Tomorrow Season 2 / Supergirl Season 2 / Freedom Fighters: The Ray
    Episode(supergirl, 2, 6),
    Episode(flash, 3, 1),
    Episode(flash, 3, 2),
    Episode(flash, 3, 3),
    Episode(flash, 3, 4),
    Episode(flash, 3, 5),
    Episode(flash, 3, 6),
    Episode(flash, 3, 7),
    Episode(supergirl, 2, 7),
    Episode(arrow, 5, 1),
    Episode(arrow, 5, 2),
    Episode(legends, 2, 1),
    Episode(legends, 2, 2),
    Episode(legends, 2, 3),
    Episode(arrow, 5, 3),
    Episode(arrow, 5, 4),
    Episode(arrow, 5, 5),
    Episode(legends, 2, 4),
    Episode(arrow, 5, 6),
    Episode(legends, 2, 5),
    Episode(arrow, 5, 7),
    Episode(legends, 2, 6),
    Episode(supergirl, 2, 8),
    Episode(flash, 3, 8),
    Episode(arrow, 5, 8),
    Episode(legends, 2, 7),
    Episode(flash, 3, 9),
    Episode(arrow, 5, 9),
    Episode(arrow, 5, 10),
    Episode(legends, 2, 8),
    Episode(legends, 2, 9),
    Episode(legends, 2, 10),
    Episode(legends, 2, 11),
    Episode(supergirl, 2, 9),
    Episode(flash, 3, 10),
    Episode(supergirl, 2, 10),
    Episode(flash, 3, 11),
    Episode(arrow, 5, 11),
    Episode(supergirl, 2, 11),
    Episode(flash, 3, 12),
    Episode(arrow, 5, 12),
    Episode(supergirl, 2, 12),
    Episode(supergirl, 2, 13),
    Episode(supergirl, 2, 14),
    Episode(flash, 3, 13),
    Episode(flash, 3, 14),
    Episode(arrow, 5, 13),
    Episode(legends, 2, 12),
    Episode(legends, 2, 13),
    Episode(arrow, 5, 14),
    Episode(arrow, 5, 15),
    Episode(supergirl, 2, 15),
    Episode(flash, 3, 15),
    Episode(flash, 3, 16),
    Episode(legends, 2, 14),
    Episode(arrow, 5, 16),
    Episode(supergirl, 2, 16),
    Episode(flash, 3, 17),
    Episode(legends, 2, 15),
    Episode(arrow, 5, 17),
    Episode(supergirl, 2, 17),
    Episode(flash, 3, 18),
    Episode(flash, 3, 19),
    Episode(flash, 3, 20),
    Episode(arrow, 5, 19),
    Episode(arrow, 5, 20),
    Episode(legends, 2, 16),
    Episode(legends, 2, 17),
    Episode(supergirl, 2, 18),
    Episode(supergirl, 2, 19)
]

def main():
    root = os.getcwd()
    print(root)
    if (not(os.path.exists(SAVE_FILE))):
        file = open(SAVE_FILE, 'w')
        file.write('')
        file.close()
    file = open(SAVE_FILE, 'r')
    so_far = file.read()
    file.close()
    so_far = so_far.split("\n")
    if (len(so_far) >= len(liste)+1):
        wb.open(DWNLD_URL)
    else:
        playWith = ""
        if (len(sys.argv) == 2):
            playWith = sys.argv[1]
        (liste[len(so_far)-1]).play()
    os.chdir(root)

#load configuration
if __name__ == "__main__":
    main()
